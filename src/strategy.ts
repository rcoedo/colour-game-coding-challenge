import Board from "./board";
import { Solution, Step } from "./solution";

export type Strategy<T> = (startingBoard: Board<T>) => Solution<T>;
export type Heuristic<T> = (board: Board<T>) => number;

class AStarNode<T> {
    public readonly f;

    constructor(public readonly board: Board<T>, public readonly h: number, public readonly steps: Array<Step<T>>) {
        this.f = h + this.steps.length;
    }

    public compare(node: AStarNode<T>) {
        if (this.f < node.f) {
            return -1;
        }
        if (this.f > node.f) {
            return 1;
        }
        return 0;
    }
}

const aStar =
    <T>(heuristic: Heuristic<T>): Strategy<T> =>
        (startingBoard: Board<T>): Solution<T> => {
            const open: Array<AStarNode<T>> = [new AStarNode(startingBoard, heuristic(startingBoard), [])];
            const closed: Array<AStarNode<T>> = [];

            while (open.length > 0) {
                // https://github.com/microsoft/TypeScript/issues/30406
                const current = open.pop()!;

                // If the current board is solved, we're done.
                if (current.board.solved()) {
                    return new Solution(startingBoard, current.steps);
                }

                // Calculate every possible successor
                const successors = current.board
                    .expand()
                    .map((step) => new AStarNode(step.board, heuristic(step.board), [...current.steps, step]));

                for (let successor of successors) {
                    // If the current board is solved, we're done.
                    if (successor.board.solved()) {
                        return new Solution(startingBoard, successor.steps);
                    }

                    // If a better node exists in our open list, skip this one.
                    const openNode = open.find((node) => node.board.equals(successor.board));
                    if (openNode && openNode.f < successor.f) {
                        continue;
                    }

                    // If we already closed a better node than this, also skip this one.
                    const closedNode = closed.find((node) => node.board.equals(successor.board));
                    if (closedNode && closedNode.f < successor.f) {
                        continue;
                    }

                    // Otherwise, push our node into our sorted list of open nodes.
                    open.push(successor);
                    open.sort((a, b) => a.compare(b) * -1);
                }

                closed.push(current);
            }
            // This point should never be reached. Our problems are always solvable.
            throw new Error("The end is nigh. No solution will you find.");
        };

const bfs = () => 0;

const numberOfColours = <T>(board: Board<T>) => board.colours().length;

/**
 * Strategy that solves a Board using an A* algorithm, where the heuristic is [H(n) = 0]. This is equivalent to a breadth-first search.
 * @param startingBoard board to solve
 * @return a solution for this board.
 */
export const aStarBfs = aStar(bfs);

/**
 * Strategy that solves a Board using an A* algorithm, where the heuristic is [H(n) = <number of remaining colours in the board>]
 * @param startingBoard board to solve
 * @return a solution for this board.
 */
export const aStarNoc = aStar(numberOfColours);

/**
 * Strategy that solves a Board using a greedy algorithm
 * @param startingBoard board to solve
 * @return a solution for this board.
 */
export function greedy<T>(startingBoard: Board<T>): Solution<T> {
    const result = [];
    let currentBoard = startingBoard;
    while (!currentBoard.solved()) {
        const step = currentBoard.expand().reduce((acc, curr) => {
            return acc.board.conquered() > curr.board.conquered() ? acc : curr;
        });
        currentBoard = step.board;
        result.push(step);
    }
    return new Solution(startingBoard, result);
}
