import Point from "./point";

export default class Board<ColourType> {
    public static readonly OUT_OF_BOUNDS = Symbol("OUT_OF_BOUNDS");
    public static readonly ORIGIN = new Point(0, 0);

    private boardSize: number;
    private _data: Array<ColourType>;
    private _colours: Set<ColourType>;

    /**
     * Creates a random board using a size and a list of possible colours.
     * @param size the board size.
     * @param colours array containing every possible colour.
     * @return the new board.
     */
    static random<T>(size: number, colours: Array<T>): Board<T> {
        if (size < 1 || !Number.isInteger(size)) {
            throw new Error("Size must be an integer");
        }

        if (colours.length === 0) {
            throw new Error("The colour list cannot be empty");
        }

        const data = Array.from(new Array(size * size)).map(() => colours[Math.floor(Math.random() * colours.length)]);

        return new Board(data);
    }

    /**
     * Creates a board
     * @param data data to use. Must have length x, where x = n^2, with n being an integer.
     */
    constructor(data: Array<ColourType>) {
        this.boardSize = Math.sqrt(data.length);

        if (!Number.isInteger(this.boardSize) || this.boardSize === 0) {
            throw new Error("Data is invalid");
        }

        this._colours = new Set(data);
        this._data = data;
    }

    private cellOutOfBounds(p: Point) {
        return p.x < 0 || p.y < 0 || p.x >= this.boardSize || p.y >= this.boardSize;
    }

    private getCellValue(p: Point) {
        return this.cellOutOfBounds(p) ? Board.OUT_OF_BOUNDS : this._data[p.x + p.y * this.boardSize];
    }

    private recursiveConqueredCells(colour: ColourType, points: Array<Point>, result: Array<Point>): Array<Point> {
        if (points.length === 0) {
            return result;
        }

        const [point, ...rest] = points;

        return this.getCellValue(point) !== colour
            ? this.recursiveConqueredCells(colour, rest, result)
            : this.recursiveConqueredCells(
                colour,
                [
                    ...rest,
                    ...point.expand().filter((p) => !this.cellOutOfBounds(p) && !p.included(result) && !p.included(rest)),
                ],
                [...result, point],
            );
    }

    private conqueredCells() {
        return this.recursiveConqueredCells(this._data[0], [Board.ORIGIN.east(), Board.ORIGIN.south()], [Board.ORIGIN]);
    }

    /**
     * Calculates a new board that is just like this one, but with the conquered zone re-coloured.
     * @param colour the new colour for the conquered zone. Must be one of the available colours.
     * @return the new board.
     */
    public transition(colour: ColourType): Board<ColourType> {
        if (!this._colours.has(colour)) {
            throw new Error("Colour is invalid");
        }

        const newData = [...this._data];

        for (let p of this.conqueredCells()) {
            newData[p.x + p.y * this.boardSize] = colour;
        }

        return new Board(newData);
    }

    /**
     * Returns the cell values for this board.
     * @return a list with the value of every cell.
     */
    public data() {
        return Array.from(this._data);
    }

    /**
     * Returns the possible colours for this board.
     * @return a list of possible colours.
     */
    public colours() {
        return Array.from(this._colours);
    }

    /**
     * Calculates the number of cells connected to the origin in this board.
     * @return the number of cells connected to the origin.
     */
    public conquered() {
        return this.conqueredCells().length;
    }

    /**
     * Returns the resulting boards of every transition possible
     * @return the list of boards.
     */
    public expand() {
        return this.colours().map((colour) => ({ colour, board: this.transition(colour) }));
    }

    /**
     * Checks if the board is solved.
     * @return true if the board is solved, false otherwise.
     */
    public solved() {
        return this.conquered() === this._data.length;
    }

    /**
     * Compares the board with another board.
     * @param board board to compare to.
     * @return true if the boards are equal, false otherwise.
     */
    public equals(board: Board<ColourType>) {
        const data = board.data();
        if (this._data.length !== data.length) {
            return false;
        }

        for (let i = 0; i < data.length; i++) {
            if (this._data[i] !== data[i]) {
                return false;
            }
        }

        return true;
    }

    /**
     * Returns the board as a string.
     * @return board string.
     */
    public toString() {
        return this._data.reduce((acc, curr, i) => `${acc}${i % this.boardSize === 0 ? "\n" : "\t"}${curr}`, "");
    }
}
