import Board from "./board";

const b = "b";
const r = "r";
const y = "y";

// prettier-ignore
const data1 = [
    b, y, r, b, r, y,
    b, r, b, y, b, y,
    b, b, y, b, b, b,
    b, y, r, b, r, y,
    b, y, r, y, y, y,
    r, b, r, y, b, r
];

// prettier-ignore
const data2 = [
    y, y, r, b, r, y,
    y, y, b, y, b, y,
    b, y, y, b, b, b,
    b, y, r, b, r, y,
    b, y, r, y, y, y,
    r, b, r, y, b, r
];

describe("Board", () => {
    describe("#constructor", () => {
        test("creates new boards from data", () => {
            expect(new Board([y]).toString()).toMatchInlineSnapshot(`
"
y"
`);
            expect(new Board(data1).toString()).toMatchInlineSnapshot(`
"
b	y	r	b	r	y
b	r	b	y	b	y
b	b	y	b	b	b
b	y	r	b	r	y
b	y	r	y	y	y
r	b	r	y	b	r"
`);

            expect(new Board(data2).toString()).toMatchInlineSnapshot(`
"
y	y	r	b	r	y
y	y	b	y	b	y
b	y	y	b	b	b
b	y	r	b	r	y
b	y	r	y	y	y
r	b	r	y	b	r"
`);
        });

        test("throws if data is invalid", () => {
            expect(() => new Board([...data1, y, b, r])).toThrowErrorMatchingInlineSnapshot(`"Data is invalid"`);
            expect(() => new Board([])).toThrowErrorMatchingInlineSnapshot(`"Data is invalid"`);
        });
    });

    describe("#conquered", () => {
        test("returns the number of cells connected to the origin by colour", () => {
            expect(new Board([1]).conquered()).toEqual(1);
            expect(new Board(data1).conquered()).toEqual(6);
            expect(new Board(data2).conquered()).toEqual(8);
        });
    });

    describe("#transition", () => {
        test("returns a new board with the conquered area changed to the transitioned colour", () => {
            expect(new Board(data1).transition(y).toString()).toMatchInlineSnapshot(`
"
y	y	r	b	r	y
y	r	b	y	b	y
y	y	y	b	b	b
y	y	r	b	r	y
y	y	r	y	y	y
r	b	r	y	b	r"
`);
            expect(new Board(data2).transition(r).toString()).toMatchInlineSnapshot(`
"
r	r	r	b	r	y
r	r	b	y	b	y
b	r	r	b	b	b
b	r	r	b	r	y
b	r	r	y	y	y
r	b	r	y	b	r"
`);
        });

        test("throws when transitioning to an invalid colour", () => {
            expect(() => new Board(data1).transition("invalid")).toThrowErrorMatchingInlineSnapshot(`"Colour is invalid"`);
        });
    });

    describe("#colours", () => {
        test("returns the array of available colours", () => {
            expect(new Board([1]).colours()).toEqual(expect.arrayContaining([1]));
            expect(new Board([1, 2, 3, 4]).colours()).toEqual(expect.arrayContaining([1, 2, 3, 4]));
            expect(new Board(data1).colours()).toEqual(expect.arrayContaining([y, b, r]));
        });
    });

    describe("#solved", () => {
        test("returns true if the board is solved, false otherwise", () => {
            expect(new Board([1]).solved()).toBe(true);
            expect(new Board(data1).solved()).toBe(false);
            expect(new Board([y, y, y, y]).solved()).toBe(true);
        });
    });

    describe("#expand", () => {
        test("returns every possible board transition", () => {
            expect(new Board(data1).expand().map(({ colour, board }) => ({ colour, board: board.toString() })))
                .toMatchInlineSnapshot(`
Array [
  Object {
    "board": "
b	y	r	b	r	y
b	r	b	y	b	y
b	b	y	b	b	b
b	y	r	b	r	y
b	y	r	y	y	y
r	b	r	y	b	r",
    "colour": "b",
  },
  Object {
    "board": "
y	y	r	b	r	y
y	r	b	y	b	y
y	y	y	b	b	b
y	y	r	b	r	y
y	y	r	y	y	y
r	b	r	y	b	r",
    "colour": "y",
  },
  Object {
    "board": "
r	y	r	b	r	y
r	r	b	y	b	y
r	r	y	b	b	b
r	y	r	b	r	y
r	y	r	y	y	y
r	b	r	y	b	r",
    "colour": "r",
  },
]
`);
        });
    });

    describe("#data", () => {
        test("returns a copy of the data", () => {
            const data = [y, b, b, r];
            const board = new Board(data);

            expect(board.data()).toEqual(data);
            expect(board.data()).not.toBe(data);
        });
    });

    describe("#equals", () => {
        test("return true if both boards have the same data, false otherwise", () => {
            expect(new Board([y]).equals(new Board([y]))).toBe(true);
            expect(new Board([y, b, b, y]).equals(new Board([y, y, b, b]))).toBe(false);
            expect(new Board([y, b, b, y]).equals(new Board([y]))).toBe(false);
            expect(new Board([y, b, r, y]).equals(new Board([y, b, r, y]))).toBe(true);
        });
    });

    describe("#toString", () => {
        test("pretty prints the board", () => {
            expect(new Board([y]).toString()).toMatchInlineSnapshot(`
"
y"
`);
            expect(new Board(data1).toString()).toMatchInlineSnapshot(`
"
b	y	r	b	r	y
b	r	b	y	b	y
b	b	y	b	b	b
b	y	r	b	r	y
b	y	r	y	y	y
r	b	r	y	b	r"
`);
        });
    });

    describe("#random", () => {
        test("builds a board of n*n size picking colours from a list", () => {
            expect(Board.random(2, [1])).toMatchInlineSnapshot(`
Board {
  "_colours": Set {
    1,
  },
  "_data": Array [
    1,
    1,
    1,
    1,
  ],
  "boardSize": 2,
}
`);
        });

        test("throws if size is < 1 or if it is not an integer", () => {
            expect(() => Board.random(2.5, [1])).toThrowErrorMatchingInlineSnapshot(`"Size must be an integer"`);
            expect(() => Board.random(-1, [1])).toThrowErrorMatchingInlineSnapshot(`"Size must be an integer"`);
            expect(() => Board.random(0, [1])).toThrowErrorMatchingInlineSnapshot(`"Size must be an integer"`);
        });

        test("throws if colour is an empty array", () => {
            expect(() => Board.random(2, [])).toThrowErrorMatchingInlineSnapshot(`"The colour list cannot be empty"`);
        });
    });
});
