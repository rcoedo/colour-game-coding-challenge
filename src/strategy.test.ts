import Board from "./board";
import { greedy, aStarBfs, aStarNoc } from "./strategy";

const b = "b";
const r = "r";
const y = "y";
const g = "g";

const board1 = new Board([b]);

// prettier-ignore
const board2 = new Board([
    b, y, b,
    r, y, b,
    r, y, y,
]);

// prettier-ignore
const board3 = new Board([
    y, y, b,
    y, y, b,
    y, y, y,
]);

// prettier-ignore
const board4 = new Board([
    y, r, y, y,
    b, g, r, y,
    b, g, r, r,
    b, g, r, g,
]);

// prettier-ignore
const board5 = new Board([
    y, y, y,
    b, r, r,
    r, y, y,
]);

describe("#greedy", () => {
    test("returns a valid solution to a board", () => {
        expect(greedy(board1).steps.map(({ colour }) => colour)).toEqual([]);
        expect(greedy(board2).steps.map(({ colour }) => colour)).toEqual([y, r, b]);
        expect(greedy(board3).steps.map(({ colour }) => colour)).toEqual([b]);
    });

    test("returns the new board for each step", () => {
        expect(greedy(board1).steps).toEqual([]);
        expect(greedy(board2).toString()).toMatchInlineSnapshot(`
"
b	y	b
r	y	b
r	y	y

=> Picking colour y

y	y	b
r	y	b
r	y	y

=> Picking colour r

r	r	b
r	r	b
r	r	r

=> Picking colour b

b	b	b
b	b	b
b	b	b 

A Solution was found in 3 steps:
	y => r => b
"
`);
        expect(greedy(board3).toString()).toMatchInlineSnapshot(`
"
y	y	b
y	y	b
y	y	y

=> Picking colour b

b	b	b
b	b	b
b	b	b 

A Solution was found in 1 steps:
	b
"
`);
        expect(greedy(board4).toString()).toMatchInlineSnapshot(`
"
y	r	y	y
b	g	r	y
b	g	r	r
b	g	r	g

=> Picking colour b

b	r	y	y
b	g	r	y
b	g	r	r
b	g	r	g

=> Picking colour g

g	r	y	y
g	g	r	y
g	g	r	r
g	g	r	g

=> Picking colour r

r	r	y	y
r	r	r	y
r	r	r	r
r	r	r	g

=> Picking colour y

y	y	y	y
y	y	y	y
y	y	y	y
y	y	y	g

=> Picking colour g

g	g	g	g
g	g	g	g
g	g	g	g
g	g	g	g 

A Solution was found in 5 steps:
	b => g => r => y => g
"
`);

        expect(greedy(board5).toString()).toMatchInlineSnapshot(`
"
y	y	y
b	r	r
r	y	y

=> Picking colour r

r	r	r
b	r	r
r	y	y

=> Picking colour y

y	y	y
b	y	y
r	y	y

=> Picking colour r

r	r	r
b	r	r
r	r	r

=> Picking colour b

b	b	b
b	b	b
b	b	b 

A Solution was found in 4 steps:
	r => y => r => b
"
`);
    });
});

describe("#aStarBfs", () => {
    test("returns a valid solution to a board", () => {
        expect(aStarBfs(board1).steps.map(({ colour }) => colour)).toEqual([]);
        expect(aStarBfs(board2).steps.map(({ colour }) => colour)).toEqual([y, r, b]);
        expect(aStarBfs(board3).steps.map(({ colour }) => colour)).toEqual([b]);
    });

    test("returns the new board for each step", () => {
        expect(aStarBfs(board1).steps).toEqual([]);
        expect(aStarBfs(board2).toString()).toMatchInlineSnapshot(`
"
b	y	b
r	y	b
r	y	y

=> Picking colour y

y	y	b
r	y	b
r	y	y

=> Picking colour r

r	r	b
r	r	b
r	r	r

=> Picking colour b

b	b	b
b	b	b
b	b	b 

A Solution was found in 3 steps:
	y => r => b
"
`);
        expect(aStarBfs(board3).toString()).toMatchInlineSnapshot(`
"
y	y	b
y	y	b
y	y	y

=> Picking colour b

b	b	b
b	b	b
b	b	b 

A Solution was found in 1 steps:
	b
"
`);

        expect(aStarBfs(board4).toString()).toMatchInlineSnapshot(`
"
y	r	y	y
b	g	r	y
b	g	r	r
b	g	r	g

=> Picking colour r

r	r	y	y
b	g	r	y
b	g	r	r
b	g	r	g

=> Picking colour b

b	b	y	y
b	g	r	y
b	g	r	r
b	g	r	g

=> Picking colour y

y	y	y	y
y	g	r	y
y	g	r	r
y	g	r	g

=> Picking colour r

r	r	r	r
r	g	r	r
r	g	r	r
r	g	r	g

=> Picking colour g

g	g	g	g
g	g	g	g
g	g	g	g
g	g	g	g 

A Solution was found in 5 steps:
	r => b => y => r => g
"
`);

        expect(aStarBfs(board5).toString()).toMatchInlineSnapshot(`
"
y	y	y
b	r	r
r	y	y

=> Picking colour b

b	b	b
b	r	r
r	y	y

=> Picking colour r

r	r	r
r	r	r
r	y	y

=> Picking colour y

y	y	y
y	y	y
y	y	y 

A Solution was found in 3 steps:
	b => r => y
"
`);
    });
});

describe("#aStarNoc", () => {
    test("returns a valid solution to a board", () => {
        expect(aStarNoc(board1).steps.map(({ colour }) => colour)).toEqual([]);
        expect(aStarNoc(board2).steps.map(({ colour }) => colour)).toEqual([r, y, b]);
        expect(aStarNoc(board3).steps.map(({ colour }) => colour)).toEqual([b]);
    });

    test("returns the new board for each step", () => {
        expect(aStarNoc(board1).steps).toEqual([]);
        expect(aStarNoc(board2).toString()).toMatchInlineSnapshot(`
"
b	y	b
r	y	b
r	y	y

=> Picking colour r

r	y	b
r	y	b
r	y	y

=> Picking colour y

y	y	b
y	y	b
y	y	y

=> Picking colour b

b	b	b
b	b	b
b	b	b 

A Solution was found in 3 steps:
	r => y => b
"
`);
        expect(aStarNoc(board3).toString()).toMatchInlineSnapshot(`
"
y	y	b
y	y	b
y	y	y

=> Picking colour b

b	b	b
b	b	b
b	b	b 

A Solution was found in 1 steps:
	b
"
`);

        expect(aStarNoc(board4).toString()).toMatchInlineSnapshot(`
"
y	r	y	y
b	g	r	y
b	g	r	r
b	g	r	g

=> Picking colour r

r	r	y	y
b	g	r	y
b	g	r	r
b	g	r	g

=> Picking colour b

b	b	y	y
b	g	r	y
b	g	r	r
b	g	r	g

=> Picking colour y

y	y	y	y
y	g	r	y
y	g	r	r
y	g	r	g

=> Picking colour r

r	r	r	r
r	g	r	r
r	g	r	r
r	g	r	g

=> Picking colour g

g	g	g	g
g	g	g	g
g	g	g	g
g	g	g	g 

A Solution was found in 5 steps:
	r => b => y => r => g
"
`);

        expect(aStarNoc(board5).toString()).toMatchInlineSnapshot(`
"
y	y	y
b	r	r
r	y	y

=> Picking colour b

b	b	b
b	r	r
r	y	y

=> Picking colour r

r	r	r
r	r	r
r	y	y

=> Picking colour y

y	y	y
y	y	y
y	y	y 

A Solution was found in 3 steps:
	b => r => y
"
`);
    });
});
