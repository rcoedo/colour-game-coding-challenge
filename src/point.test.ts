import Point from "./point";

describe("#constructor", () => {
    test("creates new points given both x and y coordinates", () => {
        expect(new Point(0, 0)).toMatchInlineSnapshot(`
Point {
  "x": 0,
  "y": 0,
}
`);
        expect(new Point(-1, 1)).toMatchInlineSnapshot(`
Point {
  "x": -1,
  "y": 1,
}
`);
        expect(new Point(3, 1)).toMatchInlineSnapshot(`
Point {
  "x": 3,
  "y": 1,
}
`);
    });

    test("throws when either x or y are not integers", () => {
        expect(() => new Point(0.5, 0.5)).toThrowErrorMatchingInlineSnapshot(`"coords can only be integers"`);
        expect(() => new Point(0, 0.5)).toThrowErrorMatchingInlineSnapshot(`"coords can only be integers"`);
        expect(() => new Point(0.5, 0)).toThrowErrorMatchingInlineSnapshot(`"coords can only be integers"`);
    });
});

describe("#north", () => {
    test("returns the point north of the current point", () => {
        expect(new Point(0, 0).north()).toMatchInlineSnapshot(`
Point {
  "x": 0,
  "y": -1,
}
`);
        expect(new Point(3, 5).north()).toMatchInlineSnapshot(`
Point {
  "x": 3,
  "y": 4,
}
`);
    });
});

describe("#south", () => {
    test("returns the point south of the current point", () => {
        expect(new Point(0, 0).south()).toMatchInlineSnapshot(`
Point {
  "x": 0,
  "y": 1,
}
`);
        expect(new Point(3, 5).south()).toMatchInlineSnapshot(`
Point {
  "x": 3,
  "y": 6,
}
`);
    });
});

describe("#east", () => {
    test("returns the point east of the current point", () => {
        expect(new Point(0, 0).east()).toMatchInlineSnapshot(`
Point {
  "x": 1,
  "y": 0,
}
`);
        expect(new Point(3, 5).east()).toMatchInlineSnapshot(`
Point {
  "x": 4,
  "y": 5,
}
`);
    });
});

describe("#west", () => {
    test("returns the point west of the current point", () => {
        expect(new Point(0, 0).west()).toMatchInlineSnapshot(`
Point {
  "x": -1,
  "y": 0,
}
`);
        expect(new Point(3, 5).west()).toMatchInlineSnapshot(`
Point {
  "x": 2,
  "y": 5,
}
`);
    });
});

describe("#expand", () => {
    test("returns an array containing the top, bottom, left and right points", () => {
        expect(new Point(0, 0).expand()).toMatchInlineSnapshot(`
Array [
  Point {
    "x": 0,
    "y": -1,
  },
  Point {
    "x": 0,
    "y": 1,
  },
  Point {
    "x": 1,
    "y": 0,
  },
  Point {
    "x": -1,
    "y": 0,
  },
]
`);
    });
});

describe("#equals", () => {
    test("returns true if the points are equals and false otherwise", () => {
        expect(new Point(0, 1).equals(new Point(0, 1))).toBe(true);
        expect(new Point(0, 1).equals(new Point(2, 3))).toBe(false);
    });
});

describe("#included", () => {
    test("returns true if the point list contains the point, false otherwise", () => {
        expect(new Point(0, 0).included([new Point(1, 0), new Point(1, 1)])).toBe(false);
        expect(new Point(0, 0).included([new Point(1, 0), new Point(1, 1), new Point(0, 0)])).toBe(true);
    });
});

describe("#toString", () => {
    test("pretty prints the point", () => {
        expect(new Point(0, 0).toString()).toMatchInlineSnapshot(`"(0, 0)"`);
    });
});
