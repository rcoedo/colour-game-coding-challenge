export default class Point {
    /**
     * Creates a new Point
     * @param x x coordinate.
     * @param y y coordinate.
     */
    constructor(public readonly x: number, public readonly y: number) {
        if (!Number.isInteger(x) || !Number.isInteger(y)) {
            throw new Error("coords can only be integers");
        }
    }

    /**
     * Returns the point north to this one.
     * @return the new point.
     */
    public north() {
        return new Point(this.x, this.y - 1);
    }

    /**
     * Returns the point south to this one.
     * @return the new point.
     */
    public south() {
        return new Point(this.x, this.y + 1);
    }

    /**
     * Returns the point east to this one.
     * @return the new point.
     */
    public east() {
        return new Point(this.x + 1, this.y);
    }

    /**
     * Returns the point west to this one.
     * @return the new point.
     */
    public west() {
        return new Point(this.x - 1, this.y);
    }

    /**
     * Returns a list of every point reachable from here (north, south, east, west).
     * @return the point list.
     */
    public expand() {
        return [this.north(), this.south(), this.east(), this.west()];
    }

    /**
     * Checks if the point is the same as this point.
     * @return true if the points are equal, false otherwise.
     */
    public equals(p: Point) {
        return this.x === p.x && this.y === p.y;
    }

    /**
     * Checks if this point is included in the point list.
     * @return true if the point is included in the point list, false otherwise.
     */
    public included(points: Array<Point>) {
        return points.some((p) => this.equals(p));
    }

    /**
     * Returns the point as a string.
     * @return point string.
     */
    public toString() {
        return `(${this.x}, ${this.y})`;
    }
}
