import { solve } from "./game";
import Board from "./board";
import { Solution } from "./solution";

describe("#solve", () => {
    test("builds a board and solves it using the strategy", () => {
        const log = jest.fn();
        const strat = jest.fn(() => new Solution(new Board([1]), []));

        console.log = log;

        solve(new Board([1, 2, 3, 4]), strat);

        expect(log.mock.calls).toMatchInlineSnapshot(`
Array [
  Array [
    "
1
 

A Solution was found in 0 steps:
	
",
  ],
]
`);

        expect(strat.mock.calls).toMatchInlineSnapshot(`
Array [
  Array [
    Board {
      "_colours": Set {
        1,
        2,
        3,
        4,
      },
      "_data": Array [
        1,
        2,
        3,
        4,
      ],
      "boardSize": 2,
    },
  ],
]
`);
    });
});
