import Board from "./board";

export type Step<T> = { colour: T; board: Board<T> };

export class Solution<T> {
    public readonly solution: Array<T>;
    public readonly length;

    /**
     * Creates a solution
     * @param board board that this solution solves.
     * @param steps steps needed to solve this board.
     */
    constructor(public readonly board: Board<T>, public readonly steps: Array<Step<T>>) {
        this.solution = steps.map(({ colour }) => colour);
        this.length = steps.length;
    }

    /**
     * Returns the solution as a string.
     * @return solution string.
     */
    public toString() {
        return `${this.board.toString()}
${this.steps.map(({ colour, board }) => `\n=> Picking colour ${colour}\n${board.toString()}`).join("\n")} \n
A Solution was found in ${this.length} steps:
\t${this.solution.join(" => ")}
`;
    }
}
