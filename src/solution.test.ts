import { Solution } from "./solution";
import Board from "./board";

describe("Solution", () => {
    describe("#toString", () => {
        test("return a string suitable for pretty printing", () => {
            expect(new Solution(new Board([1, 1, 1, 1]), []).toString()).toMatchInlineSnapshot(`
"
1	1
1	1
 

A Solution was found in 0 steps:
	
"
`);

            expect(new Solution(new Board([1, 1, 2, 2]), [{ colour: 2, board: new Board([2, 2, 2, 2]) }]).toString()).
toMatchInlineSnapshot(`
"
1	1
2	2

=> Picking colour 2

2	2
2	2 

A Solution was found in 1 steps:
	2
"
`);
        });
    });
});
