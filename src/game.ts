import Board from "./board";
import { Strategy } from "./strategy";

/**
 * Solves a board using a strategy, then prints the result.
 * @param board board to solve.
 * @param strategy strategy to use.
 */
export function solve<T>(board: Board<T>, strategy: Strategy<T>) {
    const solution = strategy(board);

    console.log(solution.toString());
}
