import chalk from "chalk";
import Board from "../src/board";

const b = chalk.blue("x");
const r = chalk.red("x");
const y = chalk.yellow("x");
const g = chalk.green("x");
const p = chalk.magenta("x");
const c = chalk.cyan("x");
const w = chalk.white("x");

export const colour = {
    blue: b,
    red: r,
    yellow: y,
    green: g,
    magenta: p,
    cyan: c,
    white: w,
};

// prettier-ignore
export const boards = [
    [
        r, y, r, b, r, y,
        b, r, b, y, b, y,
        b, b, y, b, b, b,
        b, y, r, b, r, y,
        b, y, r, y, y, y,
        r, b, r, y, b, r
    ],
    [
        r, y, r, b, r, y,
        b, r, b, y, b, y,
        b, b, g, g, b, g,
        b, y, r, g, r, y,
        b, y, r, y, y, y,
        r, b, r, y, b, r
    ],
    [
        r, y, r, b, r, y,
        b, r, b, y, p, y,
        p, b, g, g, b, g,
        b, p, p, g, r, y,
        b, y, r, y, y, p,
        r, b, r, y, b, r
    ],
    [
        r, y, r, y, r, r,
        b, b, r, y, y, y,
        r, b, b, b, b, r,
        r, b, b, y, y, y,
        y, b, r, y, b, b,
        r, r, r, r, r, b,
    ]
].map(data => new Board(data));
