import chalk from "chalk";
import * as Repl from "repl";

import { boards, colour } from "./fixtures";
import Board from "../src/board";
import { solve } from "../src/game";
import { greedy, aStarBfs, aStarNoc } from "../src/strategy";

import type { Context } from "vm";

const prompt = `GAME → `;
const user = chalk.magenta(process.env.USER);

const clearRequireCache = () => {
    Object.keys(require.cache).forEach(function(key) {
        delete require.cache[key];
    });
};

const extendWith = (properties: Object, context: Context) => {
    Object.entries(properties).forEach(([k, v]) => {
        Object.defineProperty(context, k, {
            configurable: false,
            enumerable: true,
            value: v,
        });
    });
};

const initializeContext = (context: Context) => {
    clearRequireCache();
    extendWith(
        {
            solve,
            random: Board.random,
            strategies: {
                greedy,
                aStarBfs,
                aStarNoc,
            },
            boards,
            colour,
        },
        context,
    );
};

const say = (message: string) => () => console.log(message);

const sayDoc = say(`
  The context has the following variables available:
    * ${chalk.underline.bold("solve")}: Solving function. Takes a board and a strategy and prints a solution.
    * ${chalk.underline.bold(
    "random",
)}: Board generator. Takes an integer as the board size and an array of possible colours.
    * ${chalk.underline.bold("strategies")}: Object containing the available strategies.
       - ${chalk.magenta("greedy")}: Simple greedy strategy.
       - ${chalk.magenta("aStarBfs")}: A* strategy with h(n) = 0.
       - ${chalk.magenta("aStarNoc")}: A* strategy with h(n) = number of colors in the board.
    * ${chalk.underline.bold("boards")}: Object containing some example boards.
    * ${chalk.underline.bold("colour")}: Object containing some colours.
       - ${chalk.blue("blue")}
       - ${chalk.red("red")}
       - ${chalk.yellow("yellow")}
       - ${chalk.green("green")}
       - ${chalk.magenta("magenta")}
       - ${chalk.cyan("cyan")}
       - ${chalk.white("white")}

  -> To show this doc again enter \`.doc\`.
  -> To run a game use the solve function (e.g. \`solve(boards[0], strategies.greedy)\`).
`);

const sayWelcome = say(`
  Hello, ${user}!
  You're running the Colour Game REPL.
`);

const sayBye = say(`
  Goodbye, ${user}!
`);

sayWelcome();
sayDoc();

const repl = Repl.start({ prompt });

repl.defineCommand("doc", {
    help: "Get information about the loaded modules",
    action() {
        repl.clearBufferedCommand();
        sayDoc();
        repl.displayPrompt();
    },
});

initializeContext(repl.context);

repl.on("reset", initializeContext);
repl.on("exit", sayBye);
