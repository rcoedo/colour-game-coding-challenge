# The Colour game

## Rules of the game

The player is given an n x n board of tiles where each tile is given one of m colors. Each tile is connected to up to four adjacent tiles in the North, South, East, and West directions. A tile is connected to the origin (the tile in the upper left corner) if it has the same color as the origin and there is a path to the origin consisting only of tiles of this color. A player makes a move by choosing one of the m colors. After the choice is made, all tiles that are connected to the origin are changed to the chosen color. The game proceeds until all tiles of the board have the same color. The goal of the game is to change all the tiles to the same color, preferably with the fewest number of moves possible.

## The project

The code was developed using [typescript](https://www.typescriptlang.org/) 4.4.4 as the language and [jest](https://github.com/facebook/jest) as the testing library. I'm also using [chalk](https://github.com/chalk/chalk) to colour some texts.

This project includes a small interface in the form of a read-eval-print loop to interact with the game.

The project contains the following scripts:

* Run the repl: `yarn repl`
* Run the tests: `yarn test`
* Test coverage report: `yarn coverage`

Enjoy!
